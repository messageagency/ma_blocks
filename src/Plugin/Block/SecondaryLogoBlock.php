<?php
/**
 * @file
 * Contains \Drupal\ma_blocks\Plugin\Block\SecondaryLogoBlock.
 */
namespace Drupal\ma_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides my custom block.
 *
 * @Block(
 *   id = "secondary_logo_block",
 *   admin_label = @Translation("Secondary Logo Block"),
 *   category = @Translation("Blocks")
 * )
 */

class SecondaryLogoBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return array(
      'secondary_logo_path' => 'secondary_logo.svg',
    );
  }

    /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['secondary_logo_path_text'] = array(
      '#type' => 'textfield',
      '#size' => 60,
      '#maxlength' => 128,
      '#title' => $this->t('Secondary Logo'),
      '#description' => $this->t('File path of the the secondary logo in the theme, relative to the active theme directory. E.g. "images/secondary_logo.svg"'),
      '#default_value' => $this->configuration['secondary_logo_path'],
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['secondary_logo_path']
      = $form_state->getValue('secondary_logo_path_text');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return array(
      '#type' => 'markup',
      '#markup' => '<img class="secondary-logo" alt="" aria-hidden="true" src="/' . \Drupal::theme()->getActiveTheme()->getPath() . '/' . $this->configuration['secondary_logo_path'] . '" />',
    );
  }
}